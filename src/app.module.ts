import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { AuthModule } from './auth/auth.module';
import { UserBetModule } from './user-bet/user-bet.module';
import { EventModule } from './event/event.module';
import { TransactionModule } from './transaction/transaction.module';
import { BetOptionsModule } from './bet-options/bet-options.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { typeOrmConfig } from './config/typeorm.config';

@Module({
  imports: [
    EventEmitterModule.forRoot(),
    ConfigModule.forRoot({ isGlobal: true }),
    JwtModule.register({
      secret: process.env.JWT_SECRET || 'secret-key',
      signOptions: { expiresIn: process.env.JWT_EXPIRATION || '1h' },
    }),
    TypeOrmModule.forRoot(typeOrmConfig),
    AuthModule,
    TransactionModule,
    EventModule,
    UserBetModule,
    BetOptionsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
