import { Controller, Post, Body, ValidationPipe } from '@nestjs/common';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { AuthService } from './auth.service';
import { RegisterUserDto } from './dto/register-user.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Authentication')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post('/signup')
  @ApiOperation({
    summary: 'User Registration',
    description: 'Endpoint to register a new user',
  })
  @ApiResponse({ status: 201, description: 'User registration successful' })
  @ApiResponse({ status: 400, description: 'Bad request' })
  signUp(@Body(ValidationPipe) registerUserDto: RegisterUserDto) {
    return this.authService.signUp(registerUserDto);
  }

  @Post('/signin')
  @ApiOperation({
    summary: 'User Sign In',
    description: 'Endpoint to authenticate and sign in a user',
  })
  @ApiResponse({
    status: 200,
    description: 'User authentication successful',
    type: Promise,
  })
  @ApiResponse({ status: 400, description: 'Bad request' })
  signIn(
    @Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto,
  ): Promise<{ accessToken: string }> {
    return this.authService.signIn(authCredentialsDto);
  }
}
