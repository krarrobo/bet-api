import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserRepository } from './user.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { JwtService } from '@nestjs/jwt';
import { RegisterUserDto } from './dto/register-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private jwtService: JwtService,
  ) {}

  async signUp(registerUserDto: RegisterUserDto) {
    return this.userRepository.signUp(registerUserDto);
  }

  async signIn(authCredentialsDto: AuthCredentialsDto) {
    const user = await this.userRepository.validateUserPassword(
      authCredentialsDto,
    );
    if (!user) throw new UnauthorizedException('Invalid credentials');
    const accessToken = this.jwtService.sign({
      id: user.id,
      username: user.username,
      role: user.role,
    });
    return { accessToken, user };
  }
}
