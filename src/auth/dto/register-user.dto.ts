import {
  IsString,
  MinLength,
  MaxLength,
  Matches,
  IsIn,
  IsNotEmpty,
  IsDateString,
} from 'class-validator';
import { Gender, UserRole } from '@/shared/constants';
import { ApiProperty } from '@nestjs/swagger';

export class RegisterUserDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  firstName: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  lastName: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  phone: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  documentId: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  email: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  address: string;

  @ApiProperty()
  @IsDateString()
  @IsNotEmpty()
  birthDate: Date;

  @ApiProperty()
  @IsIn([Gender.MALE, Gender.FEMALE])
  gender: Gender;

  @ApiProperty()
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  username: string;

  @ApiProperty()
  @IsString()
  @MinLength(8)
  @MaxLength(20)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'Password too weak',
  })
  password: string;

  @ApiProperty()
  @IsIn([UserRole.ADMIN, UserRole.USER])
  role: UserRole;
}
