import {
  IsString,
  MinLength,
  MaxLength,
  Matches,
  IsIn,
  IsDateString,
  IsOptional,
} from 'class-validator';
import { Gender, UserRole, UserStatus } from '@/shared/constants';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateUserDto {
  @ApiProperty()
  @IsString()
  @IsOptional()
  firstName?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  lastName?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  phone?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  documentId?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  email?: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  address?: string;

  @ApiProperty()
  @IsDateString()
  @IsOptional()
  birthDate?: Date;

  @ApiProperty()
  @IsIn([Gender.MALE, Gender.FEMALE])
  @IsOptional()
  gender?: string;

  @ApiProperty()
  @IsString()
  @MinLength(4)
  @MaxLength(20)
  @IsOptional()
  username?: string;

  @ApiProperty()
  @IsString()
  @MinLength(8)
  @MaxLength(20)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'Password too weak',
  })
  @IsOptional()
  password?: string;

  @ApiProperty()
  @IsIn([UserRole.ADMIN, UserRole.USER])
  @IsOptional()
  role?: UserRole;

  @ApiProperty()
  @IsIn([UserStatus.ACTIVE, UserStatus.BLOCKED])
  @IsOptional()
  status?: UserStatus;
}
