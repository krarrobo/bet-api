import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  DeleteDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Gender, UserRole, UserStatus } from '@/shared/constants';
import { UserBet } from '@/user-bet/entities/user-bet.entity';
import { Transaction } from '@/transaction/entities/transaction.entity';
import { Exclude } from 'class-transformer';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  firstName?: string;

  @Column()
  lastName?: string;

  @Column()
  phone?: string;

  @Column()
  documentId?: string;

  @Column()
  email?: string;

  @Column()
  address?: string;

  @Column({ type: 'date' })
  birthDate?: Date;

  @Column({ type: 'enum', enum: Gender })
  gender?: string;

  @Column({ unique: true })
  username?: string;

  @Column()
  @Exclude()
  password?: string;

  @Column()
  @Exclude()
  salt?: string;

  @Column({ type: 'enum', enum: UserRole, default: UserRole.USER })
  role?: UserRole;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @Column({ type: 'boolean', default: false })
  deleted?: boolean;

  @OneToMany(() => UserBet, (bet) => bet.user)
  bets?: UserBet[];

  @OneToMany(() => Transaction, (tx) => tx.user)
  transactions?: Transaction[];

  @Column({ type: 'enum', enum: UserStatus, default: UserStatus.ACTIVE })
  status: string;

  async validatePassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt);
    return hash === this.password;
  }
}
