import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { UserRole } from '@/shared/constants';
import { JwtPayload } from '../interfaces/jwt-payload.interface';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly jwtService: JwtService,
  ) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<UserRole[]>('roles', context.getHandler());

    if (!roles) {
      return true; // If no roles are defined for the route, allow access
    }

    const request = context.switchToHttp().getRequest();
    const token = request.headers.authorization?.split(' ')[1];
    const decodedToken = <JwtPayload>this.jwtService.decode(token);

    if (decodedToken && decodedToken.role) {
      const userRole: UserRole = decodedToken.role;

      if (roles.includes(UserRole.ADMIN) && userRole === UserRole.ADMIN) {
        return true; // Allow access for admin role
      }

      if (roles.includes(UserRole.USER) && userRole === UserRole.USER) {
        return true; // Allow access for user role
      }
    }

    return false; // Deny access if the user role doesn't match the required role
  }
}
