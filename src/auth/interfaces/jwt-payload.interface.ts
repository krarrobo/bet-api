import { UserRole } from '@/shared/constants';

export interface JwtPayload {
  username: string;
  role: UserRole;
  id: number;
}
