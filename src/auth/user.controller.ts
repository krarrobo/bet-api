import {
  Body,
  Controller,
  Delete,
  Param,
  ParseIntPipe,
  Patch,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { AuthGuard } from './guards/auth.guard';
import { UserRole } from '@/shared/constants';
import { Roles } from './roles.decorator';
import { ApiResponse, ApiTags, ApiOperation, ApiParam } from '@nestjs/swagger';

@ApiTags('User')
@Controller('user')
@UseGuards(AuthGuard)
export class UserController {
  constructor(private userService: UserService) {}

  @Delete('/:id')
  @Roles(UserRole.ADMIN)
  @ApiOperation({
    summary: 'Delete User',
    description: 'Endpoint to delete a user',
  })
  @ApiParam({ name: 'id', type: Number, description: 'User ID' })
  @ApiResponse({ status: 201, description: 'User deleted successfully' })
  @ApiResponse({ status: 404, description: 'User not found' })
  delete(@Param('id', ParseIntPipe) id: number) {
    return this.userService.deleteUser(id);
  }

  @Patch('/:id')
  @UsePipes(ValidationPipe)
  @Roles(UserRole.ADMIN)
  @ApiOperation({
    summary: 'Update User',
    description: 'Endpoint to update a user',
  })
  @ApiParam({ name: 'id', type: Number, description: 'User ID' })
  @ApiResponse({ status: 200, description: 'User updated successfully' })
  @ApiResponse({ status: 404, description: 'User not found' })
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return this.userService.updateUser(id, updateUserDto);
  }

  @Patch('/block/:id')
  @Roles(UserRole.ADMIN)
  @ApiOperation({
    summary: 'Block User',
    description: 'Endpoint to block a user',
  })
  @ApiParam({ name: 'id', type: Number, description: 'User ID' })
  @ApiResponse({ status: 200, description: 'User blocked successfully' })
  @ApiResponse({ status: 404, description: 'User not found' })
  async block(@Param('id', ParseIntPipe) id: number) {
    return this.userService.blockUser(id);
  }
}
