import { Repository, DataSource, QueryFailedError } from 'typeorm';
import { User } from './entities/user.entity';
import * as bcrypt from 'bcrypt';
import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { RegisterUserDto } from './dto/register-user.dto';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { UserStatus } from '@/shared/constants';

@Injectable()
export class UserRepository extends Repository<User> {
  constructor(private dataSource: DataSource) {
    super(User, dataSource.createEntityManager());
  }
  async signUp(registerUserDto: RegisterUserDto) {
    const salt = await bcrypt.genSalt();
    const user = this.create({
      ...registerUserDto,
      salt,
      password: await this.hashPassword(registerUserDto.password, salt),
    });
    try {
      await this.save(user);
    } catch (error) {
      if (error instanceof QueryFailedError) {
        throw new BadRequestException({ message: 'Username already taken' });
      } else {
        throw new InternalServerErrorException();
      }
    }
    return user;
  }
  async validateUserPassword(authCredentialsDto: AuthCredentialsDto) {
    const { username, password } = authCredentialsDto;
    const user = await this.findOneBy({ username, status: UserStatus.ACTIVE });
    if (user && (await user.validatePassword(password))) return user;
  }

  private async hashPassword(password: string, salt: string): Promise<string> {
    return await bcrypt.hash(password, salt);
  }
}
