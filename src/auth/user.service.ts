import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import * as bcrypt from 'bcrypt';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserRole, UserStatus } from '@/shared/constants';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {}
  async findById(id: number) {
    const user = await this.userRepository.findOneBy({ id, deleted: false });
    if (!user) throw new NotFoundException('User not found!');
    return user;
  }
  async deleteUser(id: number) {
    const deletedRes = await this.userRepository.softDelete(id);
    if (!deletedRes.affected) throw new NotFoundException('User not found!');
  }
  async updateUser(id: number, userDto: UpdateUserDto) {
    const user = await this.findById(id);
    if (user.role === UserRole.ADMIN)
      throw new BadRequestException('Cannot update other admins data');
    const userData = {
      ...user,
      ...userDto,
    };
    if (userDto.password) {
      const salt = await bcrypt.genSalt();
      return this.userRepository.save({
        ...userData,
        salt,
        password: await bcrypt.hash(userDto.password, salt),
      });
    }
    const res = await this.userRepository.save({
      ...userData,
    });
    delete res.password;
    return res;
  }
  async blockUser(id: number) {
    const user = await this.findById(id);
    if (user.role === UserRole.ADMIN)
      throw new BadRequestException('Cannot block other admins');
    user.status = UserStatus.BLOCKED;
    return this.userRepository.save(user);
  }
}
