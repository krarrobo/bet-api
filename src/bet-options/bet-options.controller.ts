import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { BetOptionsService } from './bet-options.service';
import { CreateBetOptionDto } from './dto/create-bet-option.dto';
import { UpdateBetOptionDto } from './dto/update-bet-option.dto';
import { AuthGuard } from '@/auth/guards/auth.guard';
import { UserRole } from '@/shared/constants';
import { Roles } from '@/auth/roles.decorator';
import { ApiTags, ApiOperation, ApiResponse } from '@nestjs/swagger';

@ApiTags('Bet Table')
@Controller('bet-options')
@UseGuards(AuthGuard)
export class BetOptionsController {
  constructor(private readonly betOptionsService: BetOptionsService) {}

  @Post()
  @Roles(UserRole.ADMIN)
  @ApiOperation({
    summary: 'Create Bet Option',
    description: 'Endpoint to create a new bet option',
  })
  @ApiResponse({ status: 201, description: 'Bet option created successfully' })
  create(@Body(ValidationPipe) createBetOptionDto: CreateBetOptionDto) {
    return this.betOptionsService.create(createBetOptionDto);
  }

  @Get('event/:id')
  @Roles(UserRole.ADMIN, UserRole.USER)
  @ApiOperation({
    summary: 'Get Bet Options by Event ID',
    description: 'Endpoint to get all bet options for a given event ID',
  })
  @ApiResponse({ status: 200, description: 'Bet options fetched successfully' })
  findAll(@Param('id') id: string) {
    return this.betOptionsService.findAll(+id);
  }

  @Get(':id')
  @Roles(UserRole.ADMIN, UserRole.USER)
  @ApiOperation({
    summary: 'Get Bet Option by ID',
    description: 'Endpoint to get a bet option by its ID',
  })
  @ApiResponse({ status: 200, description: 'Bet option fetched successfully' })
  @ApiResponse({ status: 404, description: 'Bet option not found' })
  findOne(@Param('id') id: string) {
    return this.betOptionsService.findOne(+id);
  }

  @Patch(':id')
  @Roles(UserRole.ADMIN)
  @ApiOperation({
    summary: 'Update Bet Option',
    description: 'Endpoint to update a bet option',
  })
  @ApiResponse({ status: 200, description: 'Bet option updated successfully' })
  @ApiResponse({ status: 404, description: 'Bet option not found' })
  update(
    @Param('id') id: string,
    @Body(ValidationPipe) updateBetOptionDto: UpdateBetOptionDto,
  ) {
    return this.betOptionsService.update(+id, updateBetOptionDto);
  }

  @Delete(':id')
  @Roles(UserRole.ADMIN)
  @ApiOperation({
    summary: 'Delete Bet Option',
    description: 'Endpoint to delete a bet option',
  })
  @ApiResponse({ status: 200, description: 'Bet option deleted successfully' })
  @ApiResponse({ status: 404, description: 'Bet option not found' })
  remove(@Param('id') id: string) {
    return this.betOptionsService.remove(+id);
  }
}
