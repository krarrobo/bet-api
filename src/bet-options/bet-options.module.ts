import { Module } from '@nestjs/common';
import { BetOptionsService } from './bet-options.service';
import { BetOptionsController } from './bet-options.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BetOption } from './entities/bet-option.entity';
import { Event } from '@/event/entities/event.entity';
import { JwtService } from '@nestjs/jwt';

@Module({
  imports: [TypeOrmModule.forFeature([BetOption, Event])],
  controllers: [BetOptionsController],
  providers: [BetOptionsService, JwtService],
})
export class BetOptionsModule {}
