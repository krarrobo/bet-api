import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBetOptionDto } from './dto/create-bet-option.dto';
import { UpdateBetOptionDto } from './dto/update-bet-option.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { BetOption } from './entities/bet-option.entity';
import { Repository } from 'typeorm';
import { Event } from '@/event/entities/event.entity';

@Injectable()
export class BetOptionsService {
  constructor(
    @InjectRepository(BetOption)
    private betOptionRepository: Repository<BetOption>,
    @InjectRepository(Event)
    private eventRepository: Repository<Event>,
  ) {}

  async create(createBetOptionDto: CreateBetOptionDto) {
    const { eventId } = createBetOptionDto;
    const event = await this.eventRepository.findOneBy({ id: eventId });
    if (!event) throw new NotFoundException('Event not found!');
    const betOpt = this.betOptionRepository.create({
      ...createBetOptionDto,
      event,
    });
    return await this.betOptionRepository.save(betOpt);
  }

  findAll(eventId: number) {
    const query = this.betOptionRepository
      .createQueryBuilder('betOpt')
      .where('betOpt.eventId = :eventId', { eventId });

    return query.getMany();
  }

  findOne(id: number) {
    return this.betOptionRepository.findOneBy({ id });
  }

  async update(id: number, updateBetOptionDto: UpdateBetOptionDto) {
    let betOpt = await this.findOne(id);
    betOpt = { ...betOpt, ...updateBetOptionDto };
    return this.betOptionRepository.save(betOpt);
  }

  remove(id: number) {
    this.betOptionRepository.softDelete(id);
  }
}
