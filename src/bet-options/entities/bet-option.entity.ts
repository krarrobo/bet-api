import { Event } from '@/event/entities/event.entity';
import { UserBet } from '@/user-bet/entities/user-bet.entity';
import {
  Column,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class BetOption {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Event, (event) => event.betOptions)
  @JoinColumn()
  event: Event;

  @Column()
  name: string;

  @Column({ type: 'float' })
  odd: number;

  @OneToMany(() => UserBet, (userBet) => userBet.betOption)
  userBets: UserBet[];

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt?: Date;
}
