import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as config from 'config';

const dbConfig = config.get('db');

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'mysql',
  host: '',
  port: 3306,
  socketPath: process.env.GCP_CLOUSDQL_SOCKET_PATH,
  username: process.env.GCP_CLOUDSQL_DATABASE_USER || dbConfig.username,
  password: process.env.GCP_CLOUDSQL_DATABASE_PASS || dbConfig.password,
  database: process.env.GCP_CLOUDSQL_DATABASE_NAME || dbConfig.database,
  entities: [__dirname + '/../**/*.entity.{js,ts}'],
  synchronize: true,
};
