import { EventStatus, Sports } from '@/shared/constants';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsIn, IsOptional } from 'class-validator';

export class CreateEventDto {
  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsIn([Sports.FOOTBALL, Sports.BASKETBALL])
  sport: string;

  @ApiProperty()
  @IsOptional()
  @IsIn([EventStatus.IN_PROGRESS, EventStatus.SCHEDULED, EventStatus.ENDED])
  status: string;
}
