import { Sports } from '@/shared/constants';
import { IsIn, IsOptional } from 'class-validator';

export class EventFilterDto {
  @IsOptional()
  @IsIn([Sports.FOOTBALL, Sports.BASKETBALL])
  sport: string;
}
