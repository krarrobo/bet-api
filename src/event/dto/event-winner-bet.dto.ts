import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';

export class EventWinnerBetDto {
  @ApiProperty()
  @IsNumber()
  winningBetOptionId: number;
}
