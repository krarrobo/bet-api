import { EventStatus } from '@/shared/constants';
import { ApiProperty } from '@nestjs/swagger';
import { IsIn, IsOptional, IsString } from 'class-validator';

export class UpdateEventDto {
  @ApiProperty()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiProperty()
  @IsOptional()
  @IsIn([EventStatus.IN_PROGRESS, EventStatus.SCHEDULED, EventStatus.ENDED])
  status?: string;
}
