import { BetOption } from '@/bet-options/entities/bet-option.entity';
import { EventStatus, Sports } from '@/shared/constants';
import { UserBet } from '@/user-bet/entities/user-bet.entity';
import {
  Column,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Event {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ type: 'enum', enum: EventStatus, default: EventStatus.SCHEDULED })
  status: string;

  @Column({ type: 'enum', enum: Sports })
  sport: string;

  @OneToMany(() => UserBet, (bet) => bet.user)
  bets: UserBet[];

  @OneToMany(() => BetOption, (betOpt) => betOpt.event)
  betOptions: BetOption[];

  @OneToOne(() => BetOption, (betOpt) => betOpt.event)
  @JoinColumn()
  winningBetOption: BetOption;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt?: Date;
}
