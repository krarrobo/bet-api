import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  ValidationPipe,
  Query,
} from '@nestjs/common';
import { EventService } from './event.service';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { Roles } from '@/auth/roles.decorator';
import { UserRole } from '@/shared/constants';
import { AuthGuard } from '@/auth/guards/auth.guard';
import { EventFilterDto } from './dto/event-filter.dto';
import { EventWinnerBetDto } from './dto/event-winner-bet.dto';
import { ApiOperation, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Event')
@Controller('event')
@UseGuards(AuthGuard)
export class EventController {
  constructor(private readonly eventService: EventService) {}

  @Post()
  @Roles(UserRole.ADMIN)
  @ApiOperation({
    summary: 'Create Event',
    description: 'Endpoint to create a new event',
  })
  @ApiResponse({ status: 201, description: 'Event created successfully' })
  create(@Body() createEventDto: CreateEventDto) {
    return this.eventService.create(createEventDto);
  }

  @Get()
  @Roles(UserRole.ADMIN, UserRole.USER)
  @ApiOperation({
    summary: 'Get All Events',
    description: 'Endpoint to get all events',
  })
  @ApiQuery({
    name: 'sport',
    type: String,
    required: false,
    description: 'Sport event filter can be football or basketball',
  })
  @ApiResponse({ status: 200, description: 'Events fetched successfully' })
  findAll(@Query(ValidationPipe) filterDto: EventFilterDto) {
    return this.eventService.findAll(filterDto);
  }

  @Get(':id')
  @Roles(UserRole.ADMIN, UserRole.USER)
  @ApiOperation({
    summary: 'Get Event by ID',
    description: 'Endpoint to get an event by its ID',
  })
  @ApiResponse({ status: 200, description: 'Event fetched successfully' })
  @ApiResponse({ status: 404, description: 'Event not found' })
  findOne(@Param('id') id: string) {
    return this.eventService.findOne(+id);
  }

  @Patch(':id')
  @Roles(UserRole.ADMIN)
  @ApiOperation({
    summary: 'Update Event',
    description: 'Endpoint to update an event',
  })
  @ApiResponse({ status: 200, description: 'Event updated successfully' })
  @ApiResponse({ status: 404, description: 'Event not found' })
  update(
    @Param('id') id: string,
    @Body(ValidationPipe) updateEventDto: UpdateEventDto,
  ) {
    return this.eventService.update(+id, updateEventDto);
  }

  @Patch('/:id/winner')
  @Roles(UserRole.ADMIN)
  @ApiOperation({
    summary: 'Set Winner Bet',
    description: 'Endpoint to set the winner bet for an event',
  })
  @ApiResponse({ status: 200, description: 'Winner bet set successfully' })
  @ApiResponse({ status: 404, description: 'Event not found' })
  setWinnerBet(
    @Param('id') id: string,
    @Body() eventWinnerDto: EventWinnerBetDto,
  ) {
    return this.eventService.setWinnerBet(+id, eventWinnerDto);
  }

  @Delete(':id')
  @Roles(UserRole.ADMIN)
  @ApiOperation({
    summary: 'Delete Event',
    description: 'Endpoint to delete an event',
  })
  @ApiResponse({ status: 200, description: 'Event deleted successfully' })
  @ApiResponse({ status: 404, description: 'Event not found' })
  remove(@Param('id') id: string) {
    return this.eventService.remove(+id);
  }
}
