import { Module } from '@nestjs/common';
import { EventService } from './event.service';
import { EventController } from './event.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Event } from './entities/event.entity';
import { JwtService } from '@nestjs/jwt';
import { BetOption } from '@/bet-options/entities/bet-option.entity';
import { TransactionModule } from '@/transaction/transaction.module';
import { TransactionService } from '@/transaction/transaction.service';
import { UserBet } from '@/user-bet/entities/user-bet.entity';
import { UserBetModule } from '@/user-bet/user-bet.module';
import { UserBetService } from '@/user-bet/user-bet.service';
import { Transaction } from '@/transaction/entities/transaction.entity';
import { EventWinnerListener } from './listeners/event-winner.listener';

@Module({
  imports: [
    TypeOrmModule.forFeature([Event, BetOption, UserBet, Transaction]),
    TransactionModule,
    UserBetModule,
  ],
  controllers: [EventController],
  providers: [
    EventService,
    JwtService,
    TransactionService,
    UserBetService,
    EventWinnerListener,
  ],
})
export class EventModule {}
