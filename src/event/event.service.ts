import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { Event } from './entities/event.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { EventFilterDto } from './dto/event-filter.dto';
import { BetOption } from '@/bet-options/entities/bet-option.entity';
import { EventWinnerBetDto } from './dto/event-winner-bet.dto';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { EventStatus } from '@/shared/constants';
import { EventWinner } from './events/event-winner.event';
import { NotFoundError } from 'rxjs';

@Injectable()
export class EventService {
  constructor(
    @InjectRepository(Event)
    private eventRepository: Repository<Event>,
    @InjectRepository(BetOption)
    private betOptionRepository: Repository<BetOption>,
    private eventEmitter: EventEmitter2,
  ) {}

  create(createEventDto: CreateEventDto) {
    const event = this.eventRepository.create(createEventDto);
    return this.eventRepository.save(event);
  }

  findAll(filterDto: EventFilterDto) {
    const query = this.eventRepository.createQueryBuilder('event');
    query.where('event.sport = :sport', { sport: filterDto.sport });
    return query.getMany();
  }

  findOne(id: number) {
    return this.eventRepository.findOneBy({ id });
  }

  async update(id: number, updateEventDto: UpdateEventDto) {
    const event = await this.findOne(id);
    if (!event) throw new NotFoundException('Event not found!');
    event.status = updateEventDto.status;
    event.name = updateEventDto.name;
    return await this.eventRepository.save(event);
  }

  async setWinnerBet(eventId: number, eventWinnerDto: EventWinnerBetDto) {
    // Update winnerBet
    const { winningBetOptionId } = eventWinnerDto;
    const event = await this.findOne(eventId);
    const winningBetOption = await this.betOptionRepository
      .createQueryBuilder('betOpt')
      .leftJoinAndSelect('betOpt.event', 'event')
      .where('betOpt.id = :id', { id: winningBetOptionId })
      .getOne();
    if (!winningBetOption || eventId !== winningBetOption.event.id)
      throw new NotFoundException('Winning Bet Option not found!');
    event.winningBetOption = winningBetOption;
    event.status = EventStatus.ENDED;
    // Dispatch event
    const evtWinner = new EventWinner();
    evtWinner.winningBetOptionId = winningBetOptionId;
    evtWinner.eventId = eventId;
    this.eventEmitter.emit('event.winner', evtWinner);
    return await this.eventRepository.save(event);
  }

  remove(id: number) {
    this.eventRepository.softDelete(id);
  }
}
