import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { EventWinner } from '../events/event-winner.event';
import { TransactionService } from '@/transaction/transaction.service';
import { UserBetService } from '@/user-bet/user-bet.service';
import { creditWinnersTransactions } from '../utils/winner-selector';
import { BetResult } from '@/shared/constants';

@Injectable()
export class EventWinnerListener {
  constructor(
    private readonly userBetService: UserBetService,
    private readonly transactionService: TransactionService,
  ) {}
  @OnEvent('event.winner')
  async handleEventWinner(eventWinner: EventWinner) {
    const { eventId, winningBetOptionId } = eventWinner;
    const bets = await this.userBetService.findAllByEvent(eventId);
    if (bets.length) {
      const winnerBets = bets.filter(
        (userBet) => userBet.betOption.id === winningBetOptionId,
      );
      if (winnerBets.length) {
        const transactions = creditWinnersTransactions(winnerBets);
        console.log(transactions);
        await this.userBetService.settleUserBets(winnerBets, BetResult.WON);
        await this.transactionService.bulkCreate(transactions);
      }
      const lostBets = bets.filter(
        (userBet) => userBet.betOption.id !== winningBetOptionId,
      );
      if (lostBets.length) {
        await this.userBetService.settleUserBets(lostBets, BetResult.LOST);
      }
    }
  }
}
