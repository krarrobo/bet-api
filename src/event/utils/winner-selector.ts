import { TxCategory, TxStatus } from '@/shared/constants';
import { Transaction } from '@/transaction/entities/transaction.entity';
import { UserBet } from '@/user-bet/entities/user-bet.entity';

export const creditWinnersTransactions = (bets: UserBet[]): Transaction[] => {
  return bets.map((userBet) => {
    const tx = new Transaction();
    tx.user = userBet.user;
    tx.amount = userBet.odd * userBet.amount;
    tx.category = TxCategory.WINNING;
    tx.status = TxStatus.CONFIRMED;
    tx.userBet = userBet;
    return tx;
  });
};
