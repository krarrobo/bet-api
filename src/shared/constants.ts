export enum UserRole {
  ADMIN = 'admin',
  USER = 'user',
}

export enum UserStatus {
  ACTIVE = 'active',
  BLOCKED = 'blocked',
}

export enum Gender {
  MALE = 'male',
  FEMALE = 'female',
}

export enum TxCategory {
  DEPOSIT = 'deposit',
  WITHDRAW = 'widthdraw',
  BET = 'bet',
  WINNING = 'winning',
}

export enum TxStatus {
  CONFIRMED = 'confirmed',
  REJECTED = 'rejected',
  IN_PROGRESS = 'in_progress',
}

export enum EventStatus {
  SCHEDULED = 'scheduled',
  IN_PROGRESS = 'in_progress',
  ENDED = 'ended',
}

export enum BetStatus {
  ACTIVE = 'active',
  CANCELLED = 'cancelled',
  SETTLED = 'settled',
}

export enum BetResult {
  WON = 'won',
  LOST = 'lost',
}

export enum Sports {
  FOOTBALL = 'football',
  BASKETBALL = 'basketball',
}
