import { IsIn, IsNumber, IsOptional } from 'class-validator';
import { TxCategory } from '@/shared/constants';
import { UserBet } from '@/user-bet/entities/user-bet.entity';
import { ApiProperty } from '@nestjs/swagger';

export class CreateTransactionDto {
  @ApiProperty()
  @IsOptional()
  userBet?: UserBet;
  @ApiProperty()
  @IsNumber()
  amount: number;
  @ApiProperty()
  @IsIn([
    TxCategory.DEPOSIT,
    TxCategory.BET,
    TxCategory.WINNING,
    TxCategory.WITHDRAW,
  ])
  category: string;
}
