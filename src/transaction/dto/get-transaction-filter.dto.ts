import { IsOptional, IsIn } from 'class-validator';
import { TxCategory } from '@/shared/constants';
export class GetTransactionFilterDto {
  @IsOptional()
  @IsIn([
    TxCategory.DEPOSIT,
    TxCategory.BET,
    TxCategory.WINNING,
    TxCategory.WITHDRAW,
  ])
  category: TxCategory;
}
