import { IsIn, IsOptional } from 'class-validator';
import { TxStatus } from '@/shared/constants';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateTransactionDto {
  @ApiProperty()
  @IsOptional()
  @IsIn([TxStatus.CONFIRMED, TxStatus.REJECTED])
  status: TxStatus;
}
