import { User } from '@/auth/entities/user.entity';
import { TxCategory, TxStatus } from '@/shared/constants';
import { UserBet } from '@/user-bet/entities/user-bet.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  DeleteDateColumn,
  UpdateDateColumn,
  JoinColumn,
} from 'typeorm';

@Entity()
export class Transaction {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.transactions)
  @JoinColumn()
  user: User;

  @Column()
  amount: number;

  @Column({ type: 'enum', enum: TxCategory, nullable: false })
  category: string;

  @Column({ type: 'enum', enum: TxStatus, default: TxStatus.IN_PROGRESS })
  status: string;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  createdAt?: Date;

  @UpdateDateColumn()
  updatedAt?: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @ManyToOne(() => UserBet, (userBet) => userBet.transactions, {
    nullable: true,
  })
  userBet?: UserBet;
}
