import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  ValidationPipe,
  Query,
} from '@nestjs/common';
import { TransactionService } from './transaction.service';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { AuthGuard } from '@nestjs/passport';
import { User } from '@/auth/entities/user.entity';
import { GetUser } from '@/auth/get-user.decorator';
import { GetTransactionFilterDto } from './dto/get-transaction-filter.dto';
import { ApiTags, ApiOperation, ApiResponse, ApiQuery } from '@nestjs/swagger';

@ApiTags('Transaction')
@Controller('transaction')
@UseGuards(AuthGuard())
export class TransactionController {
  constructor(private readonly transactionService: TransactionService) {}

  @Post()
  @ApiOperation({
    summary: 'Create Transaction',
    description: 'Endpoint to create a new transaction',
  })
  @ApiResponse({ status: 201, description: 'Transaction created successfully' })
  create(
    @Body() createTransactionDto: CreateTransactionDto,
    @GetUser() user: User,
  ) {
    return this.transactionService.create(createTransactionDto, user);
  }

  @Get()
  @ApiQuery({
    name: 'category',
    type: String,
    required: false,
    description:
      'Transaction category can be either: bet, winning, deposit or widthdraw',
  })
  @ApiOperation({
    summary: 'Get All Transactions',
    description: 'Endpoint to get all transactions',
  })
  @ApiResponse({
    status: 200,
    description: 'Transactions fetched successfully',
  })
  findAll(
    @Query(ValidationPipe) filterDto: GetTransactionFilterDto,
    @GetUser() user: User,
  ) {
    return this.transactionService.findAll(filterDto, user);
  }

  @Get(':id')
  @ApiOperation({
    summary: 'Get Transaction by ID',
    description: 'Endpoint to get a transaction by its ID',
  })
  @ApiResponse({ status: 200, description: 'Transaction fetched successfully' })
  @ApiResponse({ status: 404, description: 'Transaction not found' })
  findOne(@Param('id') id: string) {
    return this.transactionService.findOne(+id);
  }

  @Get('/user/balance')
  @ApiOperation({
    summary: 'Get User Balance',
    description: 'Endpoint to get the balance of a user',
  })
  @ApiResponse({
    status: 200,
    description: 'User balance fetched successfully',
  })
  getBalance(@GetUser() user: User) {
    return this.transactionService.getBalance(user);
  }

  @Patch(':id')
  @ApiOperation({
    summary: 'Update Transaction',
    description: 'Endpoint to update a transaction',
  })
  @ApiResponse({ status: 200, description: 'Transaction updated successfully' })
  @ApiResponse({ status: 404, description: 'Transaction not found' })
  update(
    @Param('id') id: string,
    @Body() updateTransactionDto: UpdateTransactionDto,
  ) {
    return this.transactionService.update(+id, updateTransactionDto);
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'Delete Transaction',
    description: 'Endpoint to delete a transaction',
  })
  @ApiResponse({ status: 200, description: 'Transaction deleted successfully' })
  @ApiResponse({ status: 404, description: 'Transaction not found' })
  remove(@Param('id') id: string) {
    return this.transactionService.remove(+id);
  }
}
