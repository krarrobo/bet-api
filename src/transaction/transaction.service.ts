import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { Transaction } from './entities/transaction.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '@/auth/entities/user.entity';
import { GetTransactionFilterDto } from './dto/get-transaction-filter.dto';
import { calculateBalance, canCreate } from './utils/transaction-helper';
import { TxCategory, TxStatus } from '@/shared/constants';

@Injectable()
export class TransactionService {
  constructor(
    @InjectRepository(Transaction)
    private transactionRepository: Repository<Transaction>,
  ) {}

  async create(createTransactionDto: CreateTransactionDto, user: User) {
    const { category } = createTransactionDto;
    if (category === TxCategory.WITHDRAW || category === TxCategory.BET) {
      const { balance } = await this.getBalance(user);
      if (!canCreate(createTransactionDto, balance))
        throw new BadRequestException('Not enough credit!');
    }
    const tx = this.transactionRepository.create({
      ...createTransactionDto,
      user,
      status: TxStatus.CONFIRMED,
    });
    await this.transactionRepository.save(tx);
    delete tx.user;
    return tx;
  }

  async bulkCreate(transactions: Array<Transaction>) {
    return this.transactionRepository.save(transactions);
  }

  async findAll(filterDto: GetTransactionFilterDto, user: User) {
    const { category } = filterDto;
    const query = this.transactionRepository.createQueryBuilder('transaction');
    query.where('transaction.userId = :userId', { userId: user.id });
    if (category)
      query.andWhere('transaction.category = :category', { category });
    query.addOrderBy('createdAt', 'DESC');
    return await query.getMany();
  }

  async findOne(id: number) {
    const tx = await this.transactionRepository.findOneBy({ id });
    if (!tx) throw new NotFoundException('Transaction not found!');
    return tx;
  }

  async update(id: number, updateTransactionDto: UpdateTransactionDto) {
    let tx = await this.findOne(id);
    tx = { ...tx, ...updateTransactionDto };
    return this.transactionRepository.save(tx);
  }

  async getBalance(user: User) {
    const query = this.transactionRepository.createQueryBuilder('transaction');
    query.where('transaction.userId = :userId', { userId: user.id });
    query.andWhere('transaction.status = :status', {
      status: TxStatus.CONFIRMED,
    });
    const txs = await query.getMany();
    return { balance: calculateBalance(txs) };
  }

  remove(id: number) {
    this.transactionRepository.softDelete(id);
  }
}
