import { TxCategory, TxStatus } from '@/shared/constants';
import { calculateBalance, canCreate } from './transaction-helper';
import { User } from '@/auth/entities/user.entity';

describe('Transaction helper', () => {
  const txs = [
    {
      id: 1,
      amount: 100,
      category: TxCategory.DEPOSIT,
      user: new User(),
      status: TxStatus.CONFIRMED,
    },
    {
      id: 1,
      amount: 10,
      category: TxCategory.BET,
      user: new User(),
      status: TxStatus.CONFIRMED,
    },
    {
      id: 1,
      amount: 20,
      category: TxCategory.WINNING,
      user: new User(),
      status: TxStatus.CONFIRMED,
    },
  ];

  describe('Calculate balance', () => {
    it('should calculate the balance ', () => {
      const balance = calculateBalance(txs);
      expect(balance).toBe(110);
    });
  });

  describe('Widthdraw money', () => {
    describe('when user has enough credit', () => {
      it('allows transaction', () => {
        const tx = { category: TxCategory.WITHDRAW, amount: 110 };
        const balance = calculateBalance(txs);
        expect(balance).toBe(110);
        expect(canCreate(tx, balance)).toBe(true);
      });
    });
    describe('when user has not enough credit', () => {
      it('allows transaction', () => {
        const tx = { category: TxCategory.WITHDRAW, amount: 1000 };
        const balance = calculateBalance(txs);
        expect(balance).toBe(110);
        expect(canCreate(tx, balance)).toBe(false);
      });
    });
  });
});
