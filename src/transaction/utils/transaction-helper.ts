import { TxCategory } from '@/shared/constants';
import { Transaction } from '../entities/transaction.entity';
import { CreateTransactionDto } from '../dto/create-transaction.dto';

export const calculateBalance = (txs: Array<Transaction>) => {
  return txs.reduce((acc, current) => {
    const { category, amount } = current;

    return category === TxCategory.WINNING || category === TxCategory.DEPOSIT
      ? acc + amount
      : acc - amount;
  }, 0);
};

export const canCreate = (
  createTxDto: CreateTransactionDto,
  balance: number,
) => {
  const { category, amount } = createTxDto;
  if (category === TxCategory.WITHDRAW || category === TxCategory.BET) {
    return balance - amount >= 0;
  }
  return true;
};
