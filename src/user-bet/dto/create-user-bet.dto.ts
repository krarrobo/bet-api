import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsPositive } from 'class-validator';

export class CreateUserBetDto {
  @ApiProperty()
  @IsNumber()
  betOptionId: number;
  @ApiProperty()
  @IsNumber()
  @IsPositive()
  amount: number;
}
