import { BetStatus } from '@/shared/constants';
import { ApiProperty } from '@nestjs/swagger';
import { IsIn } from 'class-validator';

export class UpdateUserBetDto {
  @ApiProperty()
  @IsIn([BetStatus.ACTIVE, BetStatus.CANCELLED])
  status: string;
}
