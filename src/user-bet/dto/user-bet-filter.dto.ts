import { Sports } from '@/shared/constants';
import { IsIn, IsNumberString, IsOptional } from 'class-validator';

export class UserBetFilter {
  @IsOptional()
  @IsNumberString()
  eventId: string;
  @IsOptional()
  @IsNumberString()
  userId: string;
  @IsOptional()
  @IsIn([Sports.BASKETBALL, Sports.FOOTBALL])
  sport: string;
}
