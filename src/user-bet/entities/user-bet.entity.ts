import { User } from '@/auth/entities/user.entity';
import { BetOption } from '@/bet-options/entities/bet-option.entity';
import { Event } from '@/event/entities/event.entity';
import { Transaction } from '@/transaction/entities/transaction.entity';
import { BetResult, BetStatus } from '@/shared/constants';
import {
  Column,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class UserBet {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.bets)
  @JoinColumn()
  user: User;

  @ManyToOne(() => Event, (event) => event.bets)
  @JoinColumn()
  event: Event;

  @ManyToOne(() => BetOption, (betOption) => betOption.userBets)
  @JoinColumn()
  betOption: BetOption;

  @OneToMany(() => Transaction, (transaction) => transaction.userBet)
  transactions: Transaction[];

  @Column({ type: 'enum', enum: BetStatus, default: BetStatus.ACTIVE })
  status: string;

  @Column({ type: 'enum', enum: BetResult, nullable: true })
  result?: string;

  @Column({ type: 'float' })
  amount: number;

  @Column({ type: 'float' })
  odd: number;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt?: Date;
}
