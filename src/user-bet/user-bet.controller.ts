import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  ValidationPipe,
  Query,
} from '@nestjs/common';
import { UserBetService } from './user-bet.service';
import { CreateUserBetDto } from './dto/create-user-bet.dto';
import { UpdateUserBetDto } from './dto/update-user-bet.dto';
import { GetUser } from '@/auth/get-user.decorator';
import { User } from '@/auth/entities/user.entity';
import { AuthGuard } from '@nestjs/passport';
import { UserBetFilter } from './dto/user-bet-filter.dto';
import { ApiTags, ApiOperation, ApiResponse, ApiQuery } from '@nestjs/swagger';

@ApiTags('User Bet')
@Controller('user-bet')
@UseGuards(AuthGuard())
export class UserBetController {
  constructor(private readonly userBetService: UserBetService) {}

  @Post()
  @ApiOperation({
    summary: 'Create User Bet',
    description: 'Endpoint to create a new user bet',
  })
  @ApiResponse({ status: 201, description: 'User bet created successfully' })
  @ApiResponse({ status: 403, description: 'Could not place bet' })
  create(
    @Body(ValidationPipe) createUserBetDto: CreateUserBetDto,
    @GetUser() user: User,
  ) {
    return this.userBetService.create(user, createUserBetDto);
  }

  @Get()
  @ApiOperation({
    summary: 'Get All User Bets',
    description: 'Endpoint to get all user bets',
  })
  @ApiQuery({
    name: 'eventId',
    type: String,
    required: false,
    description: 'Filter user bets by event',
  })
  @ApiQuery({
    name: 'sport',
    type: String,
    required: false,
    description: 'Filter user bets by sport',
  })
  @ApiQuery({
    name: 'userId',
    type: String,
    required: false,
    description: 'Filter user bets by user',
  })
  @ApiResponse({ status: 200, description: 'User bets fetched successfully' })
  findAll(@Query(ValidationPipe) filterDto: UserBetFilter) {
    return this.userBetService.findAll(filterDto);
  }

  @Get(':id')
  @ApiOperation({
    summary: 'Get User Bet by ID',
    description: 'Endpoint to get a user bet by its ID',
  })
  @ApiResponse({ status: 200, description: 'User bet fetched successfully' })
  @ApiResponse({ status: 404, description: 'User bet not found' })
  findOne(@Param('id') id: string) {
    return this.userBetService.findOne(+id);
  }

  @Patch(':id')
  @ApiOperation({
    summary: 'Update User Bet',
    description: 'Endpoint to update a user bet',
  })
  @ApiResponse({ status: 200, description: 'User bet updated successfully' })
  @ApiResponse({ status: 404, description: 'User bet not found' })
  update(
    @Param('id') id: string,
    @Body(ValidationPipe) updateUserBetDto: UpdateUserBetDto,
  ) {
    return this.userBetService.update(+id, updateUserBetDto);
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'Delete User Bet',
    description: 'Endpoint to delete a user bet',
  })
  @ApiResponse({ status: 200, description: 'User bet deleted successfully' })
  @ApiResponse({ status: 404, description: 'User bet not found' })
  remove(@Param('id') id: string) {
    return this.userBetService.remove(+id);
  }
}
