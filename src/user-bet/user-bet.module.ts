import { Module } from '@nestjs/common';
import { UserBetService } from './user-bet.service';
import { UserBetController } from './user-bet.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserBet } from './entities/user-bet.entity';
import { JwtService } from '@nestjs/jwt';
import { TransactionModule } from '@/transaction/transaction.module';
import { TransactionService } from '@/transaction/transaction.service';
import { Transaction } from '@/transaction/entities/transaction.entity';
import { User } from '@/auth/entities/user.entity';
import { BetOption } from '@/bet-options/entities/bet-option.entity';
import { AuthModule } from '@/auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserBet, Transaction, User, BetOption]),
    TransactionModule,
    AuthModule,
  ],
  controllers: [UserBetController],
  providers: [UserBetService, JwtService, TransactionService],
})
export class UserBetModule {}
