import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateUserBetDto } from './dto/create-user-bet.dto';
import { UpdateUserBetDto } from './dto/update-user-bet.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { UserBet } from './entities/user-bet.entity';
import { Repository } from 'typeorm';
import { TransactionService } from '@/transaction/transaction.service';
import { BetResult, BetStatus, TxCategory } from '@/shared/constants';
import { User } from '@/auth/entities/user.entity';
import { BetOption } from '@/bet-options/entities/bet-option.entity';
import { UserBetFilter } from './dto/user-bet-filter.dto';

@Injectable()
export class UserBetService {
  constructor(
    @InjectRepository(UserBet)
    private userBetRepository: Repository<UserBet>,
    @InjectRepository(BetOption)
    private betOptRepository: Repository<BetOption>,
    private readonly transactionService: TransactionService,
  ) {}
  async create(user: User, createUserBetDto: CreateUserBetDto) {
    const { amount, betOptionId } = createUserBetDto;
    try {
      const transaction = await this.transactionService.create(
        { category: TxCategory.BET, amount },
        user,
      );
      const betOption = await this.betOptRepository.findOne({
        where: { id: betOptionId },
        relations: ['event'],
      });

      const userBet = this.userBetRepository.create({
        betOption,
        user,
        event: betOption.event,
        odd: betOption.odd,
        amount: amount,
        transactions: [transaction],
      });

      return this.userBetRepository.save(userBet);
    } catch (error) {
      throw new BadRequestException('Could not place bet');
    }
  }

  findAll(filter: UserBetFilter) {
    const { eventId, userId, sport } = filter;
    const query = this.userBetRepository.createQueryBuilder('userBet');
    query.leftJoinAndSelect('userBet.user', 'user');
    if (userId) {
      query.where('userBet.userId = :userId', { userId });
    }
    query.leftJoinAndSelect('userBet.betOption', 'betOption');
    query.leftJoinAndSelect('userBet.event', 'event');
    query.select([
      'userBet.id',
      'userBet.amount',
      'userBet.odd',
      'userBet.status',
      'userBet.createdAt',
      'user.id',
      'user.username',
      'betOption.id',
      'betOption.name',
      'event.id',
      'event.name',
      'event.sport',
      'event.status',
    ]);
    if (eventId) query.andWhere('userBet.eventId = :eventId', { eventId });
    if (sport) query.andWhere('event.sport = :sport', { sport });
    return query.getMany();
  };

  findAllByEvent(eventId: number) {
    const query = this.userBetRepository.createQueryBuilder('userBet');
    query.where('userBet.eventId = :eventId', { eventId });
    query.andWhere('userBet.status = :status', { status: BetStatus.ACTIVE });
    query.leftJoinAndSelect('userBet.betOption', 'betOption');
    query.leftJoinAndSelect('userBet.user', 'user');
    return query.getMany();
  }

  findOne(id: number) {
    return this.userBetRepository.findOneBy({ id });
  }

  async update(id: number, updateUserBetDto: UpdateUserBetDto) {
    let userBet = await this.findOne(id);
    if (userBet.status === BetStatus.SETTLED)
      throw new BadRequestException('Cannot change settled bets');
    userBet = { ...userBet, ...updateUserBetDto };
    return this.userBetRepository.save(userBet);
  }

  async settleUserBets(userBets: UserBet[], result: BetResult) {
    return this.userBetRepository
      .createQueryBuilder()
      .update(UserBet)
      .set({ status: BetStatus.SETTLED, result })
      .where('id IN (:...ids)', { ids: userBets.map((b) => b.id) })
      .execute();
  }

  remove(id: number) {
    return this.userBetRepository.softDelete(id);
  }
}
